/**
 * 用户分页列表
 * @param query
 * @returns {*}
 */
function pageList(query) {
    return requests({
        url: '/sys/user/list',
        method: 'post',
        data: query
    })
}
