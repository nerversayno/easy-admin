


import com.mars.EasyAdminApplication;
import com.mars.common.base.UserContextInfo;
import com.mars.common.request.tool.GenCodeDeployRequest;
import com.mars.common.request.tool.GenCodeRemoveRequest;
import com.mars.framework.context.ContextUserInfoThreadHolder;
import com.mars.module.tool.request.GenTableImportRequest;
import com.mars.module.tool.service.IGenTableService;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.Collections;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = EasyAdminApplication.class)
public class GenTableTest {

    @Autowired
    private IGenTableService genTableService;

    /**
     * 部署代码测试
     */
    @Test
    public void deploy() {
        //模拟 admin 账户登录
        UserContextInfo sysUser = new UserContextInfo();
        sysUser.setUserName("admin");
        sysUser.setId(1000000000000000001L);
        ContextUserInfoThreadHolder.set(sysUser);
        //写入 gen table 数据
        String tableName = "star_cource";
        String[] tableNames = tableName.split(",");
        GenTableImportRequest request = new GenTableImportRequest();
        request.setTableNames(Arrays.asList(tableNames));
        genTableService.importGenTable(request);
        for (String table : tableNames) {
            //部署代码
            genTableService.deploy(new GenCodeDeployRequest()
                    .setTableName(Collections.singletonList(table))
                    .setType(2)
                    .setMenuId(1000000000000003001L));
        }
    }


    @Test
    public void removeTable() {
        //模拟 admin 账户登录
        UserContextInfo sysUser = new UserContextInfo();
        sysUser.setUserName("admin");
        sysUser.setId(1000000000000000001L);
        ContextUserInfoThreadHolder.set(sysUser);
        //写入 gen table 数据
        String tableName = "star_cource";
        String[] tableNames = tableName.split(",");
        for (String table : tableNames) {
            //部署代码
            genTableService.remove(new GenCodeRemoveRequest()
                    .setTableName(Collections.singletonList(table))
                    .setId(93L)
                    .setType(2)
                    .setMenuId(1000000000000003001L));
        }
    }


}
