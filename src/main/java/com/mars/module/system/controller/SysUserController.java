package com.mars.module.system.controller;

import com.mars.common.enums.BusinessType;
import com.mars.common.result.R;
import com.mars.common.request.sys.*;
import com.mars.framework.annotation.Log;
import com.mars.module.system.service.impl.SysUserServiceImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;


import lombok.AllArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;


/**
 * 用户管理控制器
 *
 * @author 源码字节-程序员Mars
 */
@Api(tags = "系统管理-用户管理")
@RestController
@RequestMapping("/sys/user")
@AllArgsConstructor
public class SysUserController {

    private final SysUserServiceImpl sysUserServiceImpl;

    /**
     * 列表查询
     *
     * @param request request
     * @return R
     */
    @PostMapping("/list")
    @ApiOperation(value = "获取列表")
    public R pageList(@RequestBody SysUserQueryRequest request) {
        return R.success(sysUserServiceImpl.pageList(request));
    }

    /**
     * 获取详情
     *
     * @param id id
     * @return R
     */
    @GetMapping("/get/{id}")
    @ApiOperation(value = "获取详情")
    public R get(@PathVariable("id") Long id) {
        return R.success(sysUserServiceImpl.get(id));
    }

    /**
     * 新增
     *
     * @param request request
     * @return R
     */
    @Log(title = "用户新增", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ApiOperation(value = "新增")
    public R add(@Validated @RequestBody SysUserAddRequest request) {
        sysUserServiceImpl.add(request);
        return R.success();
    }

    /**
     * 修改
     *
     * @param request request
     * @return R
     */
    @Log(title = "用户修改", businessType = BusinessType.UPDATE)
    @PostMapping("/update")
    @ApiOperation(value = "修改")
    public R update(@Validated @RequestBody SysUserUpdateRequest request) {
        sysUserServiceImpl.update(request);
        return R.success();
    }

    /**
     * 删除
     *
     * @param id id
     * @return R
     */
    @Log(title = "用户删除", businessType = BusinessType.DELETE)
    @PostMapping("/delete/{id}")
    @ApiOperation(value = "删除")
    public R delete(@PathVariable("id") Long id) {
        sysUserServiceImpl.delete(id);
        return R.success();
    }

}
