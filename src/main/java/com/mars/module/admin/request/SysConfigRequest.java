package com.mars.module.admin.request;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import com.mars.common.request.PageRequest;
import lombok.EqualsAndHashCode;

import java.time.LocalDateTime;
import java.util.Date;

/**
 * 系统配置请求对象 sys_config
 *
 * @author mars
 * @date 2023-11-20
 */
@Data
@ApiModel(value = "系统配置对象")
@EqualsAndHashCode(callSuper = true)
public class SysConfigRequest extends PageRequest{



    /**
     * ID
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    @ApiModelProperty(value = "id")
    private Long id;

    /**
     * 名称
     */
    @ApiModelProperty(value = "名称")
    private String name;

    /**
     * field_key
     */
    @ApiModelProperty(value = "field_key")
    private String fieldKey;

    /**
     * field_value
     */
    @ApiModelProperty(value = "field_value")
    private String fieldValue;

    /**
     * 创建时间
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "创建时间")
    private LocalDateTime createTime;
}
