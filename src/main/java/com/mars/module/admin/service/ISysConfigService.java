package com.mars.module.admin.service;

import com.mars.module.admin.entity.SysConfig;
import com.mars.common.response.PageInfo;
import com.mars.module.admin.request.SysConfigRequest;
import com.mars.module.admin.response.SysLoginResponse;

import java.util.List;

/**
 * 系统配置接口
 *
 * @author mars
 * @date 2023-11-20
 */
public interface ISysConfigService {
    /**
     * 新增
     *
     * @param param param
     * @return SysConfig
     */
    SysConfig add(SysConfigRequest param);

    /**
     * 删除
     *
     * @param id id
     * @return boolean
     */
    boolean delete(Long id);

    /**
     * 批量删除
     *
     * @param ids ids
     * @return boolean
     */
    boolean deleteBatch(List<Long> ids);

    /**
     * 更新
     *
     * @param param param
     * @return boolean
     */
    boolean update(SysConfigRequest param);

    /**
     * 查询单条数据，Specification模式
     *
     * @param id id
     * @return SysConfig
     */
    SysConfig getById(Long id);

    /**
     * 查询分页数据
     *
     * @param param param
     * @return PageInfo<SysConfig>
     */
    PageInfo<SysConfig> pageList(SysConfigRequest param);

    /**
     * 登录系统配置
     *
     * @return SysLoginResponse
     */
    SysLoginResponse sysLoginConfig(List<String> configList);

}
