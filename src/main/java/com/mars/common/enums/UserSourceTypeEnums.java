package com.mars.common.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.apache.commons.lang3.StringUtils;

/**
 * 设备类型
 * 针对多套 用户体系
 *
 * @author 源码字节-程序员Mars
 */
@Getter
@AllArgsConstructor
public enum UserSourceTypeEnums {

    /**
     * pc端
     */
    SYS_USER("sys_user"),

    /**
     * app端
     */
    APP_USER("app_user");

    private final String userType;

    public static UserSourceTypeEnums getUserType(String str) {
        for (UserSourceTypeEnums value : values()) {
            if (StringUtils.contains(str, value.getUserType())) {
                return value;
            }
        }
        throw new RuntimeException("'UserType' not found By " + str);
    }
}
